﻿function Trello() {
	$this = this;
	this.tasks = {};
	this.trello_cards = {};
	this.board = {};

	this.init = function () {
		/*this.view('trello', function (html) {
		 $('#header .header-user').prepend(html);
		 }); */

		$this.tasks = null;
		$this.trello_cards = null;
		$this.update_tasks();
	}

	this.loadTrelloCards = function () {
		$this.trello_cards = {};

		$('.list-card-title').each(function (i, e) {
			var card = $this.loadCardInfo($(e));

			if (!card) return true;

			card.node = $(e);
			$this.trello_cards[card.name] = card;
		});
	}

	this.update_tasks = function () {
		this.timecamp_api('get', 'tasks', {}, function (tasks) {
			$this.tasks = {};

			for (var t in tasks) {
				$this.tasks[tasks[t].name] = tasks[t];
			}

			$this.sync_tasks();
			$this.update_hours();
		});
	}

	this.sync_tasks = function () {
		if (!$this.board) {
			return alert("Board was not located. Please go to default Board Screen.");
		}

		$this.loadTrelloCards();

		$this.board.tc = $this.tasks[$this.board.name];

		if (!$this.board.tc) {
			$this.addTask($this.board.trello_id, $this.board.name, 0, function () {
				location.reload();
			});
		} else {
			$this.board.id = $this.board.tc.task_id;
		}

		//Only synchronized tasks on this board
		$this.tasks = filter($this.tasks, function (v, i) {
			return typeof $this.trello_cards[i] != 'undefined';
		});

		//New Trello Cards not synced
		var new_cards = filter($this.trello_cards, function (v, i) {
			return typeof $this.tasks[i] == 'undefined';
		});

		for (var t in $this.tasks) {
			$this.tasks[t].trello_card = $this.trello_cards[t].node.addClass('timecamp-indexed');
		}

		$this.addNewCards(new_cards);
	}

	this.addNewCards = function (new_cards) {
		new_cards.length = 0;

		for (var card in new_cards) {
			if (!new_cards[card].name) continue;

			new_cards.length++;
			$this.addTask(new_cards[card].id, new_cards[card].name, $this.board.id, function (tc_new_tasks) {
				$this.loadTrelloCards();

				for (var t in tc_new_tasks) {
					var name = tc_new_tasks[t].name;
					if (!$this.trello_cards[name]) {
						console.log('cards', $this.trello_cards);
						console.log('tasks', $this.tasks);
						return;
						//location.reload();
					}

					$this.tasks[name] = tc_new_tasks[t];
					$this.tasks[name].trello_card = $this.trello_cards[name].node.addClass('timecamp-indexed');
				}

				if (new_cards.length-- <= 1) {
					$this.update_hours();
				}
			});
		}
	}


	this.addTask = function (id, name, parent_id, callback) {
		if (!name) return;

		var task = {
			name:             name,
			parent_id:        parent_id,
			keywords:         id,
			external_task_id: id
		}

		$this.timecamp_api('post', 'tasks', task, function (response) {
			if (typeof callback == 'function') {
				callback(response);
			}
		});
	}

	this.getActiveCardName = function () {
		if ($('.window-title-text.current').length) {
			return $('.window-title-text.current').html().trim().replace('&amp;', '&');
		}

		return 0;
	}

	this.loadCardInfo = function (a) {
		var card = {};
		var match = a.attr('href') ? a.attr('href').match(/\/c\/([a-zA-Z0-9]*)/) : false;
		card.id = match ? 'card_' + match[1] : null;
		card.name = a.clone().children().remove().end().text().trim();

		if (!card.name) {
			return false;
		}

		return card;
	}

	this.loadBoardInfo = function () {
		if (!$this.board.name) {
			var match = document.URL.match(/\/b\/([a-zA-Z0-9]*)/);
			$this.board.trello_id = match ? 'board_' + match[1] : null;
			if ($('#content .board-header-btn-text').length) {
				$this.board.name = $('#content .board-header-btn-text').html().trim();
			}
		}

		return $this.board;
	}

	this.initCard = function () {
		var buttonList = $('.window-module.other-actions.clearfix .clearfix');

		if (!buttonList.length || $('#timecamp-track-manual').length) return;

		if (!$this.tasks) {
			return setTimeout($this.initCard, 500);
		}

		var card_name = $this.getActiveCardName();

		if (!card_name || !$this.tasks[card_name]) {
			return;
		}

		var button = $('<a/>', { 'class': 'button-link', 'id': 'timecamp-track-manual', 'status': 'unknown' });

		button.append($('<img class="timecamp" src="' + chrome.extension.getURL('images/icon-16.png') + '" />'));

		var hours = $this.tasks[card_name].total_hours || 0;

		var $add_time = $('<button id="add-manual-time" class="timecamp">+</button>'),
			$duration = $('<input type="text" id="manual-duration" class="timecamp" value="" placeholder="hours" />'),
			$hours = $('<div id="timecamp-hours" class="timecamp">Total Hours: <div class="timecamp hours">' + hours + '</div></div>');

		button.append($('<div id="trello-manual-duration" class="timecamp"><div class="timecamp load-screen"></div></div>').append($duration).append($add_time).append($hours));

		$add_time.click($this.manualClick);

		$duration.keyup(function (event) {
			if (event.keyCode == 13) {
				$this.manualClick();
			}
		});

		buttonList.prepend(button);
		$('<hr />').insertAfter('#timecamp-track-manual');
	}


	this.manualClick = function () {
		var card_name = $this.getActiveCardName();

		var task = $this.tasks[card_name];

		if (task) {
			var $duration = $('#manual-duration');
			var $hours = $('#timecamp-hours .hours')
			var time = parseFloat($duration.val());
			var duration = time * 3600;

			$duration.val('');
			$hours.html(parseFloat($hours.html()) + time);

			return $this.save_entry(task.task_id, duration)
		} else {
			msg = "There was a problem resolving the task. Please synchronize this task in TimeCamp";
		}

		alert(msg || "There was a problem communicating with TimeCamp");
	}

	this.save_entry = function (task_id, duration, note) {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1;
		var yyyy = today.getFullYear();

		dd = dd < 10 ? '0' + dd : dd;
		mm = mm < 10 ? '0' + mm : mm;
		var date = yyyy + '-' + mm + '-' + dd;

		data = {
			task_id:  task_id,
			duration: duration,
			date:     date,
			note:     note || 'Work added from Trello Board'
		}

		$this.timecamp_api('post', 'time_entry', data, function (json) {
			if (!json) {
				return alert("There was a problem adding your time entry. Please try again or add it manually on TimeCamp");
			}

			$this.update_hours();
		});
	}

	this.update_hours = function () {
		$this.timecamp_api('get', 'entries/from/2014-02-01/to/2020-03-20/', {}, function (entries) {

			//Reset Hours
			for (var t in $this.tasks) {
				$this.tasks[t].total_hours = 0;
			}

			//Add Up Hours
			if (entries.length) {
				for (var e in entries) {
					n = entries[e].name;
					if ($this.tasks[n]) {
						$this.tasks[n].total_hours += parseInt(entries[e].duration) / 3600;
					}
				}
			}

			$this.refresh_hours();
		});
	}

	this.refresh_hours = function () {
		$this.loadTrelloCards();

		for (var t in $this.tasks) {
			if (!$this.trello_cards[t]) {
				continue;
			}

			list_card = $($this.trello_cards[t].node).closest('.list-card');

			if (!list_card.find('.timecamp-hours').length) {
				list_card.append('<div class="timecamp timecamp-hours" />');
				list_card.addClass('timecamp-card');
			}

			list_card.find('.timecamp-hours').html($this.tasks[t].total_hours).show();
		}

		if (active_card = $this.getActiveCardName()) {
			$('#timecamp-hours .hours').html($this.tasks[active_card].total_hours || 0);
		}

		$('.list-area .list').not('.add-list').each(function (i, e) {
			var total = 0;

			$(e).find('.timecamp-hours').each(function (i, t) {
				total += parseFloat($(t).html());
			});

			var hours = $(e).find('.timecamp-total-hours');

			if (!hours.length) {
				hours = $('<div class="timecamp timecamp-total-hours" />').appendTo(e);
				$(e).addClass('timecamp-card-list');
			}

			hours.html(total);
		});
	}
}

Trello.prototype = new Controller();
var trello = new Trello();

$(document).ready(function () {
	startup_trello();

	//Styles
	$('body').append("<style>body.tc-loading:after { background-image: url(" + chrome.extension.getURL('images/icon-38.png') + ")");
});

function startup_trello() {
	trello.loadBoardInfo();

	if (trello.board.name) {
		trello.init();
		document.addEventListener("DOMNodeInserted", dom_updated);
	} else {
		setTimeout(startup_trello, 500);
	}
}

function dom_updated(event) {
	var node = $(event.relatedNode);

	if (node.hasClass('timecamp')) {
		return;
	}

	var old_board = trello.board.name;
	trello.board = {};
	trello.loadBoardInfo();

	if (trello.board.name != old_board) {
		return trello.init();
	}

	if (!trello.tasks) {
		return;
	}

	if ($('.window-module').length && !$('#timecamp-track-manual').length) {
		trello.initCard();
	}

	if (node.hasClass('list-cards')) {
		var added_cards = $('.list-card-title').not('.timecamp-indexed');
		if (added_cards.length) {
			var new_cards = {};

			added_cards.each(function (i, e) {
				var card = trello.loadCardInfo($(e));
				if (card.name) {
					if (trello.tasks[card.name]) {
						trello.tasks[card.name].trello_card = $(e);
					} else {
						new_cards[card.name] = card;
					}
				}
			});

			//Only add if not empty
			for (var key in new_cards) {
				if (hasOwnProperty.call(new_cards, key)) {
					return trello.addNewCards(new_cards);
				}
			}

		}

		if (!trello.refreshing) {
			trello.refreshing = true;
			trello.refresh_hours();

			//Call it again after a time delay in case of late updated node
			setTimeout(function () {
				trello.refreshing = false;
				trello.refresh_hours();
			}, 500);
		}
	}
}
