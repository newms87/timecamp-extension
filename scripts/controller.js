function Controller(){
	var $this = this;

	this.tc_url = 'https://www.timecamp.com/third_party/api/';

	this.timecamp_api = function(method, uri, data, callback) {
		$this.loading('start');

		uri += '/format/json/api_token/' + config.api_key;

		var fd = new FormData();

		if (data) {
			for (var d in data) {
				fd.append(d, data[d]);
			}
		} else {
			fd = {}
		}

		var ajax_data = {
			url:         $this.tc_url + uri,
			data:        fd,
			processData: false,
			contentType: false,
			type:        method
		};

		$.ajax(ajax_data).always(function (json) {
			$this.loading('stop');

			if (typeof callback == 'function') {
				callback(json);
			}
		});
	}

	this.loading = function(action) {
		$('body').toggleClass('tc-loading', action == 'start');

		if (action == 'stop') {
			$('#manual-duration').val('');
		}
	}

	this.view = function(view, callback){
		$.get(chrome.extension.getURL('template/'+view+'.html'), {}, function(html) {
			if (typeof callback == 'function') {
				callback(html);
			}
		}, 'html');
	}
}

var filter = function(obj, callback) {
	var new_obj = {};

	for (var i in obj) {
		if (callback(obj[i], i)) {
			new_obj[i] = obj[i]
		}
	}

	return new_obj;
}
